package com.administrarusuarios;

import com.administrarusuarios.model.Producto;
import com.administrarusuarios.service.ProductoService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/producto")
public class ProductoResource {
    private final ProductoService productoService;


    public ProductoResource(ProductoService productoService) {
        this.productoService = productoService;
    }

    @GetMapping("/all")
    public ResponseEntity<List<Producto>> getAllProductos () {
        List<Producto> productos = productoService.findAllProductos();
        return new ResponseEntity<>(productos, HttpStatus.OK);

    }

    @GetMapping("/find/{idProducto}")
    public ResponseEntity<Producto> getProductoById (@PathVariable("idProducto") Long idProducto) {
        Producto producto = productoService.findProductoById(idProducto);
        return new ResponseEntity<>(producto, HttpStatus.OK);

    }

    @PostMapping("/add")
    public ResponseEntity<Producto> addProducto(@RequestBody Producto producto) {
        Producto newProducto = productoService.addProducto(producto);
        return new ResponseEntity<>(newProducto, HttpStatus.CREATED);

    }

    @PutMapping("/update")
    public ResponseEntity<Producto> updateProducto(@RequestBody Producto producto) {
        Producto updateProducto = productoService.updateProducto(producto);
        return new ResponseEntity<>(updateProducto, HttpStatus.OK);

    }


    @DeleteMapping("/delete/{idProducto}")
    public ResponseEntity<?> deleteProducto(@PathVariable ("idProducto") Long idProducto) {
        productoService.deleteProducto(idProducto);
        return new ResponseEntity<>(HttpStatus.OK);

    }

}