package com.administrarusuarios.repository;

import com.administrarusuarios.model.Cliente;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface ClienteRepository extends JpaRepository<Cliente, Long> {
    void deleteClienteById(Long idCliente);
    Optional<Cliente> findClienteById(Long idCliente);
}
