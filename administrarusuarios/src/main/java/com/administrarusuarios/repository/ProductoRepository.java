package com.administrarusuarios.repository;

import com.administrarusuarios.model.Producto;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;


public interface ProductoRepository extends JpaRepository<Producto, Long> {
    void deleteProductoById(Long idProducto);

    Optional<Producto> findProductoById(Long idProducto);


}
