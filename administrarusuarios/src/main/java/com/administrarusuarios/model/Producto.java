package com.administrarusuarios.model;

import javax.persistence.*;
import java.io.Serializable;


@Entity

public class Producto implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(nullable = false, updatable = false)
    private Long id;
    @Column(nullable = false)
    private String nombreProducto;
    @Column(nullable = false)
    private String descripcion;
    @Column(nullable = false)
    private Integer stock;
    @Column(nullable = false)
    private Float precioVenta;

    public Producto() {}

    public Producto(String nombreProducto, String descripcion, Integer stock, Float precioVenta) {

        this.nombreProducto = nombreProducto;
        this.descripcion = descripcion;
        this.stock = stock;
        this.precioVenta = precioVenta;

    }

    public Long getIdProducto() {
        return id;
    }

    public void setIdProducto(Long idProducto) {
        this.id = idProducto;
    }

    public String getNombreProducto() {
        return nombreProducto;
    }

    public void setNombreProducto(String nombreProducto) {
        this.nombreProducto = nombreProducto;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Integer getStock() {
        return stock;
    }

    public void setStock(Integer stock) {
        this.stock = stock;
    }

    public Float getPrecioVenta() {
        return precioVenta;
    }

    public void setPrecioVenta(Float precioVenta) {
        this.precioVenta = precioVenta;
    }

    @Override
    public String toString(){
        return "Producto{" +
                ", nombre producto" + nombreProducto + '\'' +
                ", descripcion" + descripcion + '\'' +
                ", stock" + stock + '\'' +
                ", precio venta" + precioVenta + '\'' +
                '}';
    }
}
