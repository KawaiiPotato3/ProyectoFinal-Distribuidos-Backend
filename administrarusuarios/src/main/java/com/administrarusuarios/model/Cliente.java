package com.administrarusuarios.model;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Timestamp;

@Entity
public class Cliente implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(nullable = false, updatable = false)
    private Long id;
    @Column(nullable = false)
    private String nombreCliente;
    @Column(nullable = false)
    private String apellidoCliente;
    @Column(nullable = false)
    private Timestamp fechaNacimiento;
    @Column(nullable = false)
    private String celular;

    private Character genero;
    @Column(nullable = false)
    private String contrasenia;
    @Column(nullable = false)
    private String email;
    @Column(nullable = false)
    private String direccion;


    public Cliente() {}

    public Cliente(String nombreCliente, String apellidoCliente, Timestamp fechaNacimiento,
                   String celular, Character genero, String contrasenia, String email,
                   String direccion){
        this.nombreCliente = nombreCliente;
        this.apellidoCliente = apellidoCliente;
        this.fechaNacimiento = fechaNacimiento;
        this.celular = celular;
        this.genero = genero;
        this.contrasenia = contrasenia;
        this.email = email;
        this.direccion = direccion;
    }
    public Long getIdCliente() {
        return id;
    }

    public void setIdCliente(Long idCliente) {
        this.id = idCliente;
    }

    public String getNombreCliente() {
        return nombreCliente;
    }

    public void setNombreCliente(String nombreCliente) {
        this.nombreCliente = nombreCliente;
    }

    public String getApellidoCliente() {
        return apellidoCliente;
    }

    public void setApellidoCliente(String apellidoCliente) {
        this.apellidoCliente = apellidoCliente;
    }

    public Timestamp getFechaNacimiento() {
        return fechaNacimiento;
    }

    public void setFechaNacimiento(Timestamp fechaNacimiento) {
        this.fechaNacimiento = fechaNacimiento;
    }

    public String getCelular() {
        return celular;
    }

    public void setCelular(String celular) {
        this.celular = celular;
    }

    public String getContrasenia() {
        return contrasenia;
    }

    public void setContrasenia(String contrasenia) {
        this.contrasenia = contrasenia;
    }

    public Character getGenero() {
        return genero;
    }

    public void setGenero(Character genero) {
        this.genero = genero;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    @Override
    public String toString(){
        return  "Cliente{" +
                "idCliente=" + id +
                ", nombreCliente='" + nombreCliente + '\'' +
                ", apellidoCliente='" + apellidoCliente + '\'' +
                ", fechaNacimiento='" + fechaNacimiento + '\'' +
                ", celular='" + celular + '\'' +
                ", genero='" + genero + '\'' +
                ", contrasenia='" + contrasenia + '\'' +
                ", email='" + email + '\'' +
                ", direccion='" + direccion + '\'' + '}';
    }


}
