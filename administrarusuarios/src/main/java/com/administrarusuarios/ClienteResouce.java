package com.administrarusuarios;

import com.administrarusuarios.model.Cliente;
import com.administrarusuarios.service.ClienteService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/Clientes")
public class ClienteResouce {
    private final ClienteService clienteService;

    public ClienteResouce(ClienteService clienteService){
        this.clienteService=clienteService;
    }

    @GetMapping("/all")
    public ResponseEntity<List<Cliente>> getAllClientes(){
        List<Cliente> clientes = clienteService.findAllClientes();
        return new ResponseEntity<>(clientes, HttpStatus.OK);
    }
    @GetMapping("/find/{idCliente}")
    public ResponseEntity<Cliente> getClienteById(@PathVariable("idCliente") Long idCliente){
        Cliente cliente = clienteService.findClienteById(idCliente);
        return new ResponseEntity<>(cliente, HttpStatus.OK);
    }

    @PostMapping("/add")
    public ResponseEntity<Cliente> addCliente(@RequestBody Cliente cliente){
        Cliente  newcliente = clienteService.addCliente(cliente);
        return new ResponseEntity<>(newcliente, HttpStatus.CREATED);
    }

    @PutMapping("/update")
    public ResponseEntity<Cliente> updateEmployee(@RequestBody Cliente cliente) {
        Cliente updateCliente = clienteService.updateClientes(cliente);
        return new ResponseEntity<>(updateCliente, HttpStatus.OK);
    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity<?> deleteCliente(@PathVariable("id") Long idCliente) {
        clienteService.deleteCliente(idCliente);
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
