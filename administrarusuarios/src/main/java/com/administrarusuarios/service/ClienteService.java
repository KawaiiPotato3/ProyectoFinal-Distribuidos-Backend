package com.administrarusuarios.service;

import com.administrarusuarios.exception.UserNotFountException;
import com.administrarusuarios.model.Cliente;
import com.administrarusuarios.repository.ClienteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
@Transactional
public class ClienteService {
    private final ClienteRepository clienteRepository;

    @Autowired
    public ClienteService(ClienteRepository clienteRepository) {
        this.clienteRepository = clienteRepository;
    }

    public Cliente addCliente(Cliente cliente) {
        cliente.setDireccion(cliente.getDireccion() + " N° 150");
        return clienteRepository.save(cliente);
    }

    public List<Cliente> findAllClientes (){
        return clienteRepository.findAll();
    }

    public Cliente updateClientes(Cliente cliente){
        return clienteRepository.save(cliente);
    }

    public Cliente findClienteById(Long idCliente){
        return clienteRepository.findClienteById(idCliente)
                .orElseThrow(()->new UserNotFountException("El cliente numero "+idCliente+" no es encontrado"));
    }
    public void deleteCliente(Long idCliente){
        clienteRepository.deleteClienteById(idCliente);
    }









}
