package com.administrarusuarios.service;


import com.administrarusuarios.exception.UserNotFountException;
import com.administrarusuarios.model.Producto;
import com.administrarusuarios.repository.ProductoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;


@Service
public class ProductoService {
    private final ProductoRepository productoRepository;


    @Autowired
    public ProductoService(ProductoRepository productoRepository) {
        this.productoRepository = productoRepository;
    }

    public Producto addProducto(Producto producto) {
        producto.setNombreProducto(UUID.randomUUID().toString());
        return productoRepository.save(producto);

    }

    public List<Producto> findAllProductos(){
        return productoRepository.findAll();

    }

    public Producto updateProducto(Producto producto){
        return productoRepository.save(producto);
    }


    public Producto findProductoById(Long idProducto){
        return productoRepository.findProductoById(idProducto)
                .orElseThrow(() -> new UserNotFountException("Producto by id " + idProducto + "no fue encontrado"));
    }

    public void deleteProducto(Long idProducto){
        productoRepository.deleteProductoById(idProducto);
    }
}
