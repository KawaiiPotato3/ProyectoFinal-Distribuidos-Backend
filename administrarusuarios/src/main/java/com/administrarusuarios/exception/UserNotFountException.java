package com.administrarusuarios.exception;

public class UserNotFountException extends RuntimeException{
    public UserNotFountException(String mensaje) {
        super(mensaje);
    }
}
